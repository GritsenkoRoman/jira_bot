import os

from jira import JIRA

_USER_VAR = "ART_USER"
_PASS_VAR = "ART_PASSWORD"


class JiraObject:
    _SERVER = "http://localhost:5000"
    _ID = os.environ[_USER_VAR]
    _PASS = os.environ[_PASS_VAR]
    _JIRA = JIRA(server=_SERVER,
                 basic_auth=(_ID,
                             _PASS
                             )
                 )
