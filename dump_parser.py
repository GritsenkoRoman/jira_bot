# !/usr/bin/env python3
import sys
import persistqueue
import threading
import json
from time import sleep
import logging as log

from task_handler import TaskHandler


_MAX_RETRY_EXECUTIONS = 2

log.basicConfig(filename="parser.log",
                format='%(levelname)7s %(asctime)s %(threadName)s:  %(message)s',
                filemode='w',
                level=log.DEBUG
                )


def threadStopCondition(func):

    def wrapper(self, pauseTime):
        while not self._stop.wait(pauseTime):
            func(self, pauseTime)

    return wrapper


class DumpParser:
    _QUEUE_PATH = "./queueDB"
    _CLEAR_ACK_PAUSE_TIME = 30
    _INPUT_PAUSE_TIME = 0.1

    tasks = persistqueue.SQLiteAckQueue(path=_QUEUE_PATH,
                                        multithreading=True
                                        )

    def __init__(self, threadsNumber):
        self._stop = threading.Event()
        self._taskHandler = TaskHandler(DumpParser.tasks,
                                        threadsNumber)
        self._queueCleaner = threading.Thread(target=self._clearAckedData,
                                              args=(DumpParser._CLEAR_ACK_PAUSE_TIME,)
                                              )

    def start(self):
        self._taskHandler.start()
        self._queueCleaner.start()
        self._addTask(DumpParser._INPUT_PAUSE_TIME)
        self._join()

    @threadStopCondition
    def _clearAckedData(self, pauseTime: float):
        DumpParser.tasks.clear_acked_data()

    @staticmethod
    def _addTaskToQueue():
        jsonData = input()
        log.info("Read task from input to queue. Task: '" + jsonData + "'")
        taskData = json.loads(jsonData)
        task = (_MAX_RETRY_EXECUTIONS, taskData)
        DumpParser.tasks.put(task)

    @threadStopCondition
    def _addTask(self, pauseTime: float):
        try:
            DumpParser._addTaskToQueue()
        except EOFError:
            self._stop.set()
            sleep(1)
        except Exception as e:
            log.warning("in DUMP_PARSER: " + str(e))
            sleep(1)

    def _join(self):
        self._queueCleaner.join()
        self._taskHandler.stop()
        self._taskHandler.join()


def main():
    try:
        threadNumber = int(sys.argv[1])
        dumpParser = DumpParser(threadNumber)
        dumpParser.start()
    except Exception as e:
        log.warning("in main: " + str(e))
        exit()


if __name__ == "__main__":
    main()

