import logging as log
from abc import ABC,\
                abstractmethod
from threading import Thread,\
                      Event
from time import sleep


_STOP_WAIT_TIME = 1


def threadStopCondition(func):
    def wrapper(self):
        while not self._stopEvent.wait(_STOP_WAIT_TIME) or not self._queue.empty():
            func(self)

    return wrapper


class ThreadHandlerBase(ABC):
    def __init__(self, queue):
        self._queue = queue
        self._stopEvent = Event()
        self._worker = Thread(target=self._handleTask)

    @abstractmethod
    def _handleQueueMember(self, value):
        pass

    @threadStopCondition
    def _handleTask(self):
        try:
            value = self._queue.get()
        except Exception as e:
            sleep(1)
            log.info("Error in "+self.__class__.__name__ + str(e))
        else:
            self._handleQueueMember(value)

    def put(self, value):
        log.info(self.__class__.__name__ + "        put")
        self._queue.put(value)

    def start(self):
        log.info(self.__class__.__name__ + "        start")
        self._worker.start()

    def join(self):
        log.info(self.__class__.__name__ + "        join")
        self._worker.join()

    def stop(self):
        log.info(self.__class__.__name__ + "        stop")
        self._stopEvent.set()
