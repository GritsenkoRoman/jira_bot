from comment import Comment
from issue import Issue


class NoDumpCommandsError(Exception):
    pass


def createDumpTask(commentId, issueId) -> {}:
    comment = Comment(commentId, issueId)
    if comment.hasDumpCommands():
        return comment.getJsonDumpCommands()
    else:
        raise NoDumpCommandsError()


def addErrorComment(message, issueId):
    issue = Issue(issueId)
    issue.addComment(message)

