import logging as log
import os

from thread_handler_base import ThreadHandlerBase
from jira_object import JiraObject
from messages import MessageObject,\
                     AttachmentMessage


class _CommentAdder(JiraObject):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(_CommentAdder, cls).__new__(cls)
        return cls.instance

    def add(self, message: MessageObject):
        if isinstance(message, AttachmentMessage):
            self._addAttachmentMessage(message)
        elif isinstance(message, MessageObject):
            self._addMessage(message)

    def _addMessage(self, message: MessageObject):
        self._JIRA.add_comment(message.issueId, str(message))

    def _addAttachmentMessage(self, message: AttachmentMessage):
        for fileDirectory, filename in zip(message.fileDirectories,
                                           message.filenames):
            filePath = os.path.join(fileDirectory, filename)
            self._JIRA.add_attachment(message.issueId, filePath)
            os.remove(filePath)
        self._JIRA.add_comment(message.issueId, str(message))


class MessageHandler(ThreadHandlerBase):
    commentAdder = _CommentAdder()

    def _handleQueueMember(self, message: MessageObject):
        log.info("Handle message: " + message.__class__.__name__ + "\n")
        MessageHandler.commentAdder.add(message)



