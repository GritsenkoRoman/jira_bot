import re

import dump_template


BOOL_TRUE_WORDS = ['true', 't', 'yes', 'y', '1']
BOOL_FALSE_WORDS = ['false', 'f', 'no', 'n', '0']


class Error(Exception):
    def __init__(self, command: str=str(), message=str(), commentAuthor: str=str()):
        self.command = command
        self.commentAuthor = commentAuthor
        self.message = message

    def __str__(self):
        return self.message


class CommandTerminatorError(Error):
    def __init__(self, command: str=str(), commentAuthor: str=str()):
        message = 'Не указан знак конца команды. Попробуйте добавить "----" в конце ' \
                  'каждой команды! '
        super().__init__(command, message, commentAuthor)


class EmptyCommandError(Error):
    def __init__(self, command: str=str()):
        message = 'Пустая команда!'
        super().__init__(command, message)


class NoDumpFileError(Error):
    def __init__(self, command: str=str()):
        message = 'Не указан файл дампа!'
        super().__init__(command, message)


class UndefinedWordError(Error):
    def __init__(self, command: str=str(), errorWord: str=str()):
        message = 'Неизвестное ключевое слово: "' + errorWord + '"!'
        super().__init__(command, message)


class EmptyValueError(Error):
    def __init__(self, command: str=str(), errorWord: str=str()):
        message = 'Не указано значение у ключевого слова: "' + errorWord + '"!'
        super().__init__(command, message)


class AttachmentsError(Error):
    def __init__(self, command: str=str(), errorWord: str=str()):
        message = 'К задаче не прикреплен файл: "' + errorWord + '"!'
        super().__init__(command, message)


class NumberError(Error):
    def __init__(self, command: str=str(), errorWord: str=str()):
        message = '*' + errorWord + '* должен быть положительным числом!'
        super().__init__(command, message)


class BoolError(Error):
    def __init__(self, command: str=str(), errorWord: str=str()):
        message = '*' + errorWord + '* должен быть или *true/false*,' \
                                                 ' или *t/f*,' \
                                                 ' или *yes/no*,' \
                                                 ' или *y/n*,' \
                                                 ' или *0/1*!'
        super().__init__(command, message)


def _determineNoTerminatorCommand(commands: [str]) -> str:
    for command in commands:
        if dump_template.hasBotMention(command):
            return command
    return str()


def checkCommandsForTerminator(fullCommands: [str]):
    noTerminatorCommand = _determineNoTerminatorCommand(fullCommands)
    if noTerminatorCommand:
        errorCommand = dump_template.replaceBotIdMention(noTerminatorCommand)
        raise CommandTerminatorError(errorCommand)


def raiseWordError(errorCommandPart: str, fullCommand: str):
    template = dump_template.getFirstWord()
    errorWord = re.search(template, errorCommandPart)
    word = errorWord.group(0)
    if dump_template.isKeyword(word):
        raise EmptyValueError(fullCommand, word)
    else:
        raise UndefinedWordError(fullCommand, word)


def checkCommand(command: str, fullCommand: str):
    commandLength = len(command)
    fullCommandLength = len(fullCommand)
    if not fullCommandLength:
        raise EmptyCommandError(fullCommand)
    elif not commandLength:
        raise NoDumpFileError(fullCommand)
    elif commandLength < fullCommandLength:
        errorCommandPart = fullCommand[commandLength:]
        raiseWordError(errorCommandPart, fullCommand)


def checkAttachments(attachments: [str], issueAttachments: [str]):
    for attachment in attachments:
        if attachment not in issueAttachments:
            raise AttachmentsError(errorWord=attachment)


def checkNumber(keyword: str, word: str):
    if not (word.isdigit() and 1 <= int(word)):
        raise NumberError(errorWord=keyword)


def isBool(word: str):
    return word.lower() in (BOOL_TRUE_WORDS + BOOL_FALSE_WORDS)


def checkBool(keyword: str, word: str):
    if not isBool(word):
        raise BoolError(errorWord=keyword)

