import logging as log
import os
import shutil
import subprocess
from threading import RLock
from multiprocessing import Pool as ThreadPool
from multiprocessing import current_process,\
                            Manager

import dump_template
from jira_object import JiraObject
from thread_handler_base import ThreadHandlerBase
from message_handler import MessageHandler
from messages import BeginMessage,\
                     ErrorWhileParseDumpMessage,\
                     FailParseDumpMessage,\
                     ErrorMessage,\
                     CompleteMessage

_PROCESS_DIRECTORY_NAME = "processes"
_DUMP_DIRECTORY_NAME = "dumps"
_JOBS_DIRECTORY_NAME = "ngp.axxon/linux/coredump-report/jobs"
_LOAD_COMMAND_NAME = "./ngp.axxon/linux/coredump-report/service-get-files-from-uri.sh"
_PARSE_COMMAND_NAME = "./service_parse_dump.sh"

_USER = "ART_USER"
_PASS = "ART_PASSWORD"

_PROCESS_ID_ARG = "--process_id="
_RESULT_DIRECTORY_ARG = "--result-directory="
_DUMP_DIRECTORY_ARG = "--dump-directory="

_PROCESS_LOG_FILENAME_BASE = "parse_service"

_LOG_EXTENSION = ".log"

_manager = Manager()
_processesId = _manager.list()
_lock = RLock()


class Task:
    def __init__(self, fullTaskData, messageHandler: MessageHandler, queue):
        self.fullTaskData = fullTaskData
        self.executionsLeft, self.data = fullTaskData
        self.messageHandler = messageHandler
        self.queue = queue

    def success(self, fileNames: []):
        try:
            self._handleSuccessCase(fileNames)
        except Exception as e:
            log.warning("In success: ", str(e))

    def fail(self, error):
        try:
            self._handleFailCase(error)
        except Exception as e:
            log.warning("In fail: ", str(e))

    def _handleSuccessCase(self, fileNames: []):
        log.info("success: " + str(fileNames))
        completeComment = CompleteMessage(self.fullTaskData, fileNames)
        self.messageHandler.put(completeComment)
        self.queue.ack(self.fullTaskData)

    def _runReExecution(self, error):
        if isinstance(error, ParseError):
            errorComment = ErrorWhileParseDumpMessage(self.fullTaskData, [error.fileName])
        else:
            errorComment = ErrorMessage(self.fullTaskData, error)
        self.messageHandler.put(errorComment)
        newElement = (self.executionsLeft - 1, self.data)
        self.queue.put(newElement)

    def _stopExecution(self, error):
        if isinstance(error, ParseError):
            failComment = FailParseDumpMessage(self.fullTaskData, [error.fileName])
        else:
            failComment = ErrorMessage(self.fullTaskData, error)
        self.messageHandler.put(failComment)

    def _handleFailCase(self, error):
        log.info("fail: " + str(error) + "\n" + str(self.fullTaskData))
        if self.executionsLeft:
            self._runReExecution(error)
        else:
            self._stopExecution(error)
        self.queue.ack(self.fullTaskData)


class ParseError(Exception):
    def __init__(self, logFile: str):
        self.fileName = logFile


def _listFiles(directory, extension):
    return [os.path.join(directory, f)
            for f in os.listdir(directory)
            if f.endswith(extension)]


def _generate_log_filename(processDirectory: str, processId: int) -> str:
    files = _listFiles(processDirectory, _LOG_EXTENSION)
    i = 0
    while True:
        logName = _PROCESS_LOG_FILENAME_BASE + str(processId) + '-' + str(i) + _LOG_EXTENSION
        logFile = os.path.join(processDirectory, logName)
        if logFile not in files:
            return logName
        i += 1


class ProcessHandler(JiraObject):
    def __init__(self, task: {}):
        self.task = task
        self._setIdent()
        self._setProcessDirectory()
        self._initializeProcessDirectory()

    def run(self) -> []:
        command = self._createParseBashCommand()
        logFilename = _generate_log_filename(self.processDirectory, self._ident)
        executionLogFilePath = os.path.join(self.processDirectory, logFilename)
        with open(executionLogFilePath, 'w') as f:
            process = subprocess.run(command,
                                     stdout=subprocess.PIPE,
                                     stderr=f,
                                     universal_newlines=True
                                     )
        resultPath = self._getResultFiles(process.stdout, executionLogFilePath)
        os.remove(executionLogFilePath)
        return resultPath

    @staticmethod
    def _getResultFiles(stackFiles: str, logFilePath: str) -> []:
        files = [i for i in stackFiles.split("\n") if i]
        if not files:
            raise ParseError(logFilePath)
        else:
            return files

    @staticmethod
    def _load(files: [], directory: str):
        commandName = _LOAD_COMMAND_NAME
        command = [commandName, directory] + files
        process = subprocess.run(command,
                                 stdout=subprocess.DEVNULL,
                                 stderr=subprocess.STDOUT,
                                 universal_newlines=True
                                 )

    def _setIdent(self):
        process = current_process()
        with _lock:
            if process.ident in _processesId:
                self._ident = _processesId.index(process.ident)
            else:
                self._ident = len(_processesId)
                _processesId.append(process.ident)

    def _setProcessDirectory(self):
        projectPath = os.getcwd()
        processPath = os.path.join(projectPath, _PROCESS_DIRECTORY_NAME)
        self.processDirectory = os.path.join(processPath, str(self._ident))

    def _loadFiles(self):
        directoryKeywords = dump_template.hotfixKeywords()
        for keyword in directoryKeywords:
            if keyword in self.task:
                directoryPath = os.path.join(self.processDirectory, keyword)
                _clearFolder(directoryPath)
                self._load(self.task[keyword], directoryPath)

    def _initializeProcessDirectory(self):
        if not os.path.exists(self.processDirectory):
            os.makedirs(self.processDirectory)
        self._loadFiles()

    def _createParseBashCommand(self) -> []:
        command = []
        commandName = _PARSE_COMMAND_NAME
        command.append(commandName)
        command.append(_RESULT_DIRECTORY_ARG + self.processDirectory)
        dumpDirectory = os.path.join(self.processDirectory, _DUMP_DIRECTORY_NAME)
        _clearFolder(dumpDirectory)
        command.append(_DUMP_DIRECTORY_ARG + dumpDirectory)
        command.append(_PROCESS_ID_ARG + str(self._ident))
        for key in self.task.keys():
            if dump_template.isBoolValue(key):
                if self.task[key]:
                    command.append("--" + key)
            elif dump_template.isHotfixKeyword(key):
                hotfixDirectory = os.path.join(self.processDirectory, key)
                command.append("--" + key + "_dir=" + hotfixDirectory)
            elif key != dump_template.dumpName() and\
                    key != dump_template.issueName() and\
                    key != dump_template.authorName():
                command.append("--" + key + "=" + str(self.task[key]))
        command += self.task[dump_template.dumpName()]
        return command


def _execute(task: Task, messages):
    beginMessage = BeginMessage(task)
    messages.put(beginMessage)
    _, data = task
    log.info("\nExecute task start: \n" + "\n".join([str(i) for i in data["dump_url"]]))
    handler = ProcessHandler(data)
    result = handler.run()
    log.info("\nExecute task end:\n" + "\n".join([str(i) for i in data["dump_url"]]) +
             "\nResult:\n" + "\n".join([str(i) for i in result]))
    return result


def _clearFolder(folder: str):
    if not os.path.exists(folder):
        return
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            log.warning("Failed to delete" + str(file_path) + ". Reason: " + str(e))


class TaskHandler(ThreadHandlerBase):
    def __init__(self, queue, threadsNumber: int):
        super().__init__(queue)
        self._pool = ThreadPool(threadsNumber)
        self.messages = _manager.Queue()
        self.messageHandler = MessageHandler(self.messages)
        currentFolder = os.getcwd()
        processesDirectory = os.path.join(currentFolder, _PROCESS_DIRECTORY_NAME)
        jobsDirectory = os.path.join(currentFolder, _JOBS_DIRECTORY_NAME)
        _clearFolder(processesDirectory)
        _clearFolder(jobsDirectory)

    def start(self):
        self.messageHandler.start()
        super().start()

    def join(self):
        super().join()
        self._pool.close()
        self._pool.join()
        self.messageHandler.stop()
        self.messageHandler.join()

    def _handleQueueMember(self, task):
        log.info("Get task from persist queue and handle. " + str(task))
        newTask = Task(task, self.messageHandler, self._queue)
        self._pool.apply_async(_execute, (task, self.messages,),
                               callback=newTask.success,
                               error_callback=newTask.fail
                               )

    def _handleTask(self):
        super()._handleTask()
