#!/bin/bash
cd ngp.axxon/linux/coredump-report || exit 1
export PROCESS_ID=
while true; do
    case "$1" in
        --oem)
            OEM=${1};;
        --alt_ngp_plan=*)
            ALT_NGP_PLAN="${1}";;
        --alt_ngp_build=*)
            ALT_NGP_BUILD="${1}";;
        --alt_ngp_artifact_path=*)
            ALT_ARTIFACT_PATH="${1}";;
        --oem_drivers_build=*)
            OEM_DRIVERS_BUILD="${1}";;
        --oem_detectors_build=*)
            OEM_DETECTORS_BUILD="${1}";;
        --ngp_hotfix_dir=*)
            NGP_HOTFIX_DIR="${1}";;
        --drivers_hotfix_dir=*)
            DRIVERS_HOTFIX_DIR="${1}";;
        --detectors_hotfix_dir=*)
            DETECTORS_HOTFIX_DIR="${1}";;
        --distro=*)
            DISTRO="${1}";;
        --force_latest_container)
            FORCE_LATEST_CONTAINER="${1}";;
        --result-directory=*)
            RESULT_DIRECTORY="${1}";;
        --dump-directory=*)
            DUMP_DIRECTORY="${1#--dump-directory=}";;
        --process_id=*)
            PROCESS_ID="${1#--process_id=}";;
        -*)
            echo "ERROR: unknown option '$1'" >&2
            usage
            exit 1 ;;
        *)
            break ;;
    esac
    shift
done
./service-get-dumps-from-uri.sh "$DUMP_DIRECTORY" "$@" |\
xargs ./service-prepare-jobs.sh ${OEM}\
                                ${ALT_NGP_PLAN}\
                                ${ALT_NGP_BUILD}\
                                ${ALT_ARTIFACT_PATH}\
                                ${OEM_DRIVERS_BUILD}\
                                ${OEM_DETECTORS_BUILD}\
                                ${NGP_HOTFIX_DIR}\
                                ${DRIVERS_HOTFIX_DIR}\
                                ${DETECTORS_HOTFIX_DIR} |\
xargs ./service-process-jobs.sh ${DISTRO}\
                                ${RESULT_DIRECTORY}\
                                ${FORCE_LATEST_CONTAINER}
cd ../../..