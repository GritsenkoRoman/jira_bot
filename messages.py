import os

import dump_template


def _getIssueId(issueUrl: str):
    return issueUrl.split('/')[-1]


class MessageObject:
    def __init__(self, task):
        _, data = task
        self.issueId = _getIssueId(data[dump_template.issueName()])
        self._userId = data[dump_template.authorName()]

    def __str__(self):
        return "[~" + self._userId + "], "


class BeginMessage(MessageObject):
    def __str__(self):
        return super().__str__() + "разбор Dump начался."


class ErrorMessage(MessageObject):
    def __init__(self, task, error):
        super().__init__(task)
        self._error = error

    def __str__(self):
        return super().__str__() + "во время разбора возникла ошибка " + str(self._error) +\
               "."


class AttachmentMessage(MessageObject):
    def __init__(self, task, filePathes: []):
        super().__init__(task)
        self.fileDirectories = [os.path.dirname(i) for i in filePathes]
        self.filenames = [os.path.basename(i) for i in filePathes]

    @staticmethod
    def attachmentMentions(func):
        def wrapper(self):
            files = ""
            for filename in self.filenames:
                files += "[^" + filename + "]\n"
            return func(self) + files
        return wrapper


class ErrorWhileParseDumpMessage(AttachmentMessage):
    @AttachmentMessage.attachmentMentions
    def __str__(self):
        return super().__str__() + "разбор Dump завершился с ошибкой. Для более точной " \
                                   "информации просмотрите файл логов: "


class FailParseDumpMessage(AttachmentMessage):
    @AttachmentMessage.attachmentMentions
    def __str__(self):
        return super().__str__() + "не удалось разобрать Dump. Попробуйте переписать " \
                                   "команду. Для более точной информации просмотрите файл " \
                                   "логов: "


class CompleteMessage(AttachmentMessage):
    @AttachmentMessage.attachmentMentions
    def __str__(self):
        return super().__str__() + " разбор Dump завершился.\n" \
                                   "Результат разбора: "
