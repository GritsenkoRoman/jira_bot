import re

import dump_template
import dump_validator


class CommentParser:
    def __init__(self, issue):
        self._issue = issue
        self._setFullCommandRegex()
        self._setCommandRegex()
        self._setKeywordValueRegex()
        self._setAttachmentRegex()

    def extractDumpCommands(self, comment) -> []:
        fullCommands = self._fullCommandRegex.findall(comment)
        commands = self._commandRegex.findall(comment)
        dump_validator.checkCommandsForTerminator(fullCommands)
        responses = []
        for commandInfo in zip(commands, fullCommands):
            parsedCommand = self._parseCommand(commandInfo)
            responses.append(parsedCommand)
        return responses

    def _parseCommand(self, commandInfo) -> {}:
        try:
            return self._parseModifiedCommand(commandInfo)
        except dump_validator.Error as e:
            _, fullCommand = commandInfo
            e.command = fullCommand
            raise e

    def _parseModifiedCommand(self, commandInfo) -> {}:
        (extractedCommand, dumpKeyword), fullCommand = commandInfo
        dump_validator.checkCommand(extractedCommand, fullCommand)
        if dumpKeyword == str():
            extractedCommand = dump_template.dumpName() + extractedCommand
        parsedCommand = self._parseCommandOnKeyValue(extractedCommand)
        return parsedCommand

    def _parseCommandOnKeyValue(self, command) -> {}:
        keywordValuePairs = self._keywordValueRegex.findall(command)
        parsedData = {}
        for keyWord, values in keywordValuePairs:
            key = dump_template.modifyKeyword(keyWord)
            validatedValues = self._parseValueByKeywordType(key, values)
            parsedData[key] = validatedValues
        return parsedData

    def _parseValueByKeywordType(self, keyword: str, values: str):
        if dump_template.isAttachmentValue(keyword):
            return self._parseAttachments(values)
        elif dump_template.isNumberValue(keyword):
            dump_validator.checkNumber(keyword, values)
            return int(values)
        elif dump_template.isBoolValue(keyword):
            dump_validator.checkBool(keyword, values)
            return values in dump_validator.BOOL_TRUE_WORDS
        return values

    def _parseAttachments(self, attachments: str) -> []:
        attachmentList = self._attachmentRegex.findall(attachments)
        issueAttachmentFilenames = self._issue.getAttachmentFilenames()
        dump_validator.checkAttachments(attachmentList, issueAttachmentFilenames)
        attachmentLinks = self._getAttachmentLinks(attachmentList)
        return attachmentLinks

    def _getAttachmentLinks(self, attachments: [str]) -> []:
        resultList = []
        issueAttachments = self._issue.getAttachments()
        for filename, content in issueAttachments:
            if filename in attachments:
                resultList.append(content)
        return resultList

    def _setFullCommandRegex(self):
        fullCommandTemplate = dump_template.getFullCommand()
        self._fullCommandRegex = re.compile(fullCommandTemplate, re.IGNORECASE | re.DOTALL)

    def _setCommandRegex(self):
        commandTemplate = dump_template.getCommand()
        self._commandRegex = re.compile(commandTemplate, re.IGNORECASE | re.DOTALL)

    def _setKeywordValueRegex(self):
        keywordValueTemplate = dump_template.getKeywordValue()
        self._keywordValueRegex = re.compile(keywordValueTemplate, re.IGNORECASE | re.DOTALL)

    def _setAttachmentRegex(self):
        attachmentTemplate = dump_template.getAttachment()
        self._attachmentRegex = re.compile(attachmentTemplate)
