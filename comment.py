import json

import dump_template
import dump_validator
from jira_object import JiraObject
from comment_parser import CommentParser
from issue import Issue


class Comment(JiraObject):
    def __init__(self, commentId, issueId):
        self._comment = self._JIRA.comment(issueId, commentId)
        self._issue = Issue(issueId)

    def hasDumpCommands(self) -> bool:
        return dump_template.botMention() in self._comment.body

    def getJsonDumpCommands(self) -> []:
        dumpCommands = self._extractDumpCommands()
        jsonCommands = []
        for command in dumpCommands:
            jsonCommand = json.dumps(command)
            jsonCommands.append(jsonCommand)
        return jsonCommands

    def _createHeader(self) -> {}:
        author = self._comment.author.name
        header = {dump_template.issueName(): self._issue.getURL(),
                  dump_template.authorName(): author,
                  dump_template.oem(): False}
        return header

    def _addHeader(self, command: {}):
        header = self._createHeader()
        header.update(command)
        return header

    def _modifyCommands(self, commands: [str]) -> []:
        responses = []
        for command in commands:
            commandWithHeader = self._addHeader(command)
            responses.append(commandWithHeader)
        return responses

    def _extractDumpCommands(self) -> []:
        try:
            parser = CommentParser(self._issue)
            parsedComment = parser.extractDumpCommands(self._comment.body)
            return self._modifyCommands(parsedComment)
        except dump_validator.Error as e:
            e.commentAuthor = self._comment.author.name
            raise e

