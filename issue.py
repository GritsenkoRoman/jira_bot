from jira_object import JiraObject


class Issue(JiraObject):
    _ISSUE_URL_BASE = JiraObject._SERVER + "/browse/"

    def __init__(self, issueId):
        self._issue = Issue._JIRA.issue(issueId)

    def getURL(self) -> str:
        return self._ISSUE_URL_BASE + self._issue.key

    def getAttachments(self) -> []:
        return [(i.filename, i.content)
                for i in self._issue.fields.attachment]

    def getAttachmentFilenames(self) -> []:
        return [i.filename for i in self._issue.fields.attachment]

    def addComment(self, commentBody: str):
        self._JIRA.add_comment(self._issue, commentBody)
