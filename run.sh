#!/bin/bash

usage() {
cat <<EOF
Usage: $(basename "$0") [-h|--help] [-t|--threads=<threads>]

Params:
    -t --threads - Set number of executing threads

EOF
}

readonly THREADS_NUMBER_DEFAULT=4

if [ -n "$1" ]
then
    case $1 in
        -h | --help)
            usage
            exit 1
            ;;
        -t)
            threads_number="${2}"
            ;;
        --threads=*)
            threads_number="${1#--threads=}"
            ;;
        -*)
            echo "Error! Invalid argument!"
            exit 70
            ;;
        esac
else
    threads_number=$THREADS_NUMBER_DEFAULT
fi

if [ -z "$ART_USER" ]; then
    if [ ! -e /dev/tty ]; then
        echo "ERROR: variable ART_USER is not set" >&2
        exit 1
    fi
    read -p 'Username: ' ART_USER
    export ART_USER
fi

if [ -z "$ART_PASSWORD" ]; then
    if [ ! -e /dev/tty ]; then
        echo "ERROR: variable ART_PASSWORD is not set" >&2
        exit 1
    fi
    read -sp 'Password: ' ART_PASSWORD
    export ART_PASSWORD
fi

python3 http_server.py | python3 dump_parser.py $threads_number
