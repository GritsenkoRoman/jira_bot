#!/usr/bin/env python3
import sys
from flask import Flask,\
                  request
from waitress import serve
import json
import logging

import jira_bot
import dump_validator

app = Flask(__name__)

app.logger.disabled = True
log = logging.getLogger('werkzeug')
log.disabled = True


def handleDumpCommandException(error, issueId):
    errorMessage = "[~" + error.commentAuthor + "] " + "Неправильно написана команда:\r\n" \
                   "\r\n----\r\n" + error.command + "\r\n----\r\n" + "{color:#FF0000}" + \
                   str(error) + "{color}"
    jira_bot.addErrorComment(errorMessage, issueId)


def handleCommentCreatedWebhook(commentId, issueId):
    try:
        jsonResponses = jira_bot.createDumpTask(commentId, issueId)
        return jsonResponses
    except dump_validator.Error as e:
        handleDumpCommandException(e, issueId)


def handleWebhook(data, issueId):
    try:
        jdata = json.loads(data)
        if jdata['webhookEvent'] == 'comment_created':
            commentId = jdata['comment']['id']
            jsonResponses = handleCommentCreatedWebhook(commentId, issueId)
            for responce in jsonResponses:
                print(responce, flush=True)
    except jira_bot.NoDumpCommandsError:
        pass
    except Exception as e:
        msg = 'httpServer Error:' + str(e)
        print("\n" + msg + "\n", file=sys.stderr)


@app.route('/comment/<issueId>', methods=['POST'])
def hook(issueId):
    data = request.data
    data = data.decode('utf-8')
    handleWebhook(data, issueId)
    return ""


@app.route('/', methods=['GET'])
def index():
    return "Jira webhook listener"


if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=5001)
