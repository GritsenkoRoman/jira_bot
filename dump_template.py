import re

_BOT_ID = "gira_bot"
_BOT_ID_MENTION = "[~" + _BOT_ID + "]"

_DUMP_NAME = "dump_url"
_ISSUE_NAME = "jira_task"
_AUTHOR_NAME = "created_by"
_OEM = "oem"
_FORCE_LATEST_CONTAINER = ["force", "latest", "container"]

_NUMBER_KEYWORD_INTERSECTION = "build"
_DUMP_KEYWORD_INTERSECTION = "dump"
_HOTFIX_KEYWORD_INTERSECTION = "hotfix"
_BOOL_KEYWORD = ["_".join(_FORCE_LATEST_CONTAINER),
                 _OEM
                 ]


_KEYWORD_DELIMITERS = "(?:[- _])*"
_DELIMITERS = "\s*"

_DUMP_KEYWORD = [["linux", "dump"],
                 ["dump"],
                 ]
_HOTFIX_KEYWORD = [["ngp", "hotfix"],
                   ["drivers", "hotfix"],
                   ["detectors", "hotfix"]
                   ]
_OPTION_KEYWORD = [_FORCE_LATEST_CONTAINER,
                   ["distro"],
                   ["alt", "ngp", "artifact", "path"],
                   ["alt", "ngp", "build"],
                   ["alt", "ngp", "plan"],
                   ["oem", "drivers", "build"],
                   ["oem", "detectors", "build"],
                   [_OEM]
                   ]
_TERMINATOR = ["----",
               "[ \t\r\f\xa0]*(?:\n[ \t\r\f\xa0]*)+[.]",
               "\s*$"
               ]


def _dumpKeywordRegex() -> str:
    keywords = [_KEYWORD_DELIMITERS.join(i) for i in _DUMP_KEYWORD]
    return '|'.join(keywords)


def _hotfixKeywordRegex() -> str:
    keywords = [_KEYWORD_DELIMITERS.join(i) for i in _HOTFIX_KEYWORD]
    return '|'.join(keywords)


def _optionKeywordRegex() -> str:
    keywords = [_KEYWORD_DELIMITERS.join(i) for i in _OPTION_KEYWORD]
    return '|'.join(keywords)


def _terminatorRegex() -> str:
    return '|'.join(_TERMINATOR)


def _allKeywordsRegex() -> str:
    return _dumpKeywordRegex() + "|" + _hotfixKeywordRegex() + "|" + _optionKeywordRegex()


def botId() -> str:
    return _BOT_ID


def botMention() -> str:
    return _BOT_ID_MENTION


def dumpName() -> str:
    return _DUMP_NAME


def issueName() -> str:
    return _ISSUE_NAME


def authorName() -> str:
    return _AUTHOR_NAME


def oem() -> str:
    return _OEM


def hotfixKeywords() -> []:
    keywords = ["_".join(i) for i in _HOTFIX_KEYWORD]
    return keywords


def optionKeywords() -> []:
    keywords = ["_".join(i) for i in _OPTION_KEYWORD]
    return keywords


def dumpKeywords() -> []:
    keywords = ["_".join(i) for i in _DUMP_KEYWORD]
    return keywords


def hasBotId(command: str) -> bool:
    return _BOT_ID in command


def hasBotMention(command: str) -> bool:
    return _BOT_ID_MENTION in command


def isAttachmentValue(word: str) -> bool:
    return isDumpName(word) or \
           isHotfixKeyword(word)


def isNumberValue(word: str) -> bool:
    return _NUMBER_KEYWORD_INTERSECTION in word


def isBoolValue(word: str) -> bool:
    return word in _BOOL_KEYWORD


def isDumpName(word: str) -> bool:
    return _DUMP_NAME == word


def isHotfixKeyword(keyword: str) -> bool:
    hotfixKeywords = ["_".join(i) for i in _HOTFIX_KEYWORD]
    return keyword in hotfixKeywords


def isDumpKeyword(keyword: str) -> bool:
    dumpKeywords = [str().join(i) for i in _DUMP_KEYWORD]
    return keyword in dumpKeywords


def removeIntermediateSymbols(word: str) -> str:
    loweredKeyword = word.lower()
    return re.sub("[- _]+", str(), loweredKeyword.strip())


def isKeyword(word: str):
    replacedKeyword = removeIntermediateSymbols(word)
    keywords = _DUMP_KEYWORD + _HOTFIX_KEYWORD + _OPTION_KEYWORD
    noSpaceKeywords = [str().join(i) for i in keywords]
    return replacedKeyword in noSpaceKeywords


def modifyKeyword(keyword: str) -> str:
    replacedKeyword = removeIntermediateSymbols(keyword)
    if isDumpKeyword(replacedKeyword) or keyword == dumpName():
        return dumpName()
    keywords = _HOTFIX_KEYWORD + _OPTION_KEYWORD
    noSpaceKeywords = [str().join(i) for i in keywords]
    index = noSpaceKeywords.index(replacedKeyword)
    return "_".join(keywords[index])


def getFullCommand():
    botMention = "\[~" + botId() + "\]"
    terminator_ = "(?:" + _terminatorRegex() + ")"
    command = "(.*?)"
    spaceSymbols = "[ \t\r\f\xa0]*"
    notIncludedSymbols = spaceSymbols + "(?:\n" + spaceSymbols + ")*"
    fullCommand = "(?:" + botMention + notIncludedSymbols + command + ")" + notIncludedSymbols + terminator_
    return fullCommand


def getCommand():
    botMention = "\[~" + botId() + "\]"
    delimiters = _DELIMITERS
    dumpKeyword_ = "(?:" + _dumpKeywordRegex() + ")?"
    attachment = "(?:\[\^.*?\])"
    attachments = "(?:" + delimiters + attachment + ")"
    hotfixOptionTerminatorKeywords = "(?:" + _hotfixKeywordRegex() + "|"\
                                           + _optionKeywordRegex() + "|" \
                                           + _terminatorRegex() + ")"
    hotfixKeyword_ = "(?:" + _hotfixKeywordRegex() + ")(?!" + delimiters + hotfixOptionTerminatorKeywords + ")"
    hotfixes = "(?:" + delimiters + hotfixKeyword_ + attachments + "*" + ")*"
    optionKeyword_ = "(?:" + _optionKeywordRegex() + ")(?!" + delimiters + hotfixOptionTerminatorKeywords + ")"
    option = "(?:" + delimiters + "\S*)"
    options = "(?:" + delimiters + optionKeyword_ + option + ")*"
    command = botMention + delimiters + "(" + "(" + dumpKeyword_ + ")" + attachments + "+" + "(?:" + hotfixes + options + ")*)?"
    return command


def getKeywordValue():
    values = "(?:" + _DELIMITERS + "\[\^.*?\])+|\S+"
    keyword = dumpName() + "|" + _allKeywordsRegex()
    keywordValue = "(" + keyword + ")" + _DELIMITERS + "(" + values + ")" + _DELIMITERS
    return keywordValue


def getAttachment():
    attachment = "(?:\[\^(.*?)\])"
    return attachment


def getFirstWord():
    keywords = _allKeywordsRegex()
    firstWord = keywords + "|" + "\S+|\Z"
    return firstWord


def replaceBotIdMention(command):
    if hasBotId(command):
        return command.replace(botMention(), botId())

